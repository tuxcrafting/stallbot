from asyncio import get_event_loop
from configparser import ConfigParser
from discord import opus
from logging import basicConfig, INFO
from peewee import MySQLDatabase, PostgresqlDatabase
from playhouse.apsw_ext import APSWDatabase
from re import split
from sys import argv
from .bot import Bot
from .model import db_proxy
from .models import *
from .plugins import *
from .util import AttrDict

CONFPATH = "config.ini"

def main():
    if len(argv) != 2:
        print(f"Usage: {argv[0]} <profile>")
        return

    basicConfig(level=INFO)

    config = ConfigParser(dict_type=AttrDict)
    config.read(CONFPATH)
    pfconfig = config._sections[argv[1]]

    db_def = pfconfig.database.split(":")

    if db_def[0] == "sqlite3":
        database = APSWDatabase(db_def[1])
    elif db_def[0] in ("postgres", "mysql"):
        host, port, user, password, db = db_def[1:]
        if db_def[0] == "postgres":
            database = PostgresqlDatabase(
                db,
                user=user,
                password=password,
                host=host,
                port=int(port)
            )
        else:
            database = MySQLExtDatabase(
                db,
                user=user,
                password=password,
                host=host,
                port=int(port)
            )
    else:
        raise ValueError("Invalid database type")
    db_proxy.initialize(database)

    database.connect()
    database.create_tables([
        MarkovModel,
        TransactionModel,
        UserModel
    ], safe=True)
    database.close()

    bot = Bot(pfconfig, database)

    bot.add_plugin(BasicPlugin)
    bot.add_plugin(MathPlugin)
    bot.add_plugin(FunPlugin)
    bot.add_plugin(CryptoPlugin)
    bot.add_plugin(MoneyPlugin)
    bot.add_plugin(VoicePlugin)
    bot.add_plugin(BotAdminPlugin)

    statuslines = split(r"\s+\|\|\s+", pfconfig.statuslines.strip())
    bot.plugins["botadmin"].statuslines = statuslines

    bot.run()

    bot.plugins["money"].flush_pending()
