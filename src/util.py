from ctypes import c_long, c_ulong
from re import compile
from shlex import shlex

class BotException(Exception): pass

class CmdNotFoundException(BotException):
    def __init__(self, s):
        super(CmdNotFoundException, self).__init__(
            f"Unknown command `{s}`"
        )

class UnauthorizedBotException(BotException):
    def __init__(self):
        super(UnauthorizedBotException, self).__init__(
            "You don't have the authorization to run this command"
        )

class ArgumentBotException(BotException): pass

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

RE_INTEGER = compile(r"^[-+]?\d+$")
RE_FLOAT = compile(r"^[-+]?(\d+(\.\d*)|\.\d+)$")
RE_USERID = compile(r"^\d+$")
RE_MENTION = compile(r"^<@!?(\d+)>$")

RE_SUB = compile(r"\$\(([^$]+)\)")

def nlen(expected):
    l = [len(expected)]
    n = 0
    for x in expected:
        if isinstance(x, list):
            n += 1
            l.append(len(expected) - n)
    return l

def do_type(arg, expected):
    if isinstance(expected, list):
        if arg == "":
            return None
        expected = expected[0]
    if expected == str:
        return arg
    elif expected == int:
        if not RE_INTEGER.search(arg):
            raise ArgumentBotException("Invalid argument type")
        return int(arg)
    elif expected == float:
        if not RE_FLOAT.search(arg):
            raise ArgumentBotException("Invalid argument type")
        return float(arg)
    elif expected == id:
        if RE_USERID.search(arg):
            return arg
        elif RE_MENTION.search(arg):
            return RE_MENTION.search(arg).group(1)
        else:
            raise ArgumentBotException("Invalid user ID")

async def parse_args(bot, msg, argdata, expected):
    while True:
        m = RE_SUB.search(argdata)
        if not m:
            break
        d = await bot.exec_str(msg, m.group(1), True)
        argdata = RE_SUB.sub(d, argdata, 1)
    if len(expected) == 0:
        if argdata:
            raise ArgumentBotException("Expected 0 arguments")
        return []
    elif len(expected) == 1:
        if argdata.startswith("`") and argdata.endswith("`"):
            argdata = argdata[1:-1]
        return [do_type(argdata, expected[0])]
    else:
        lex = shlex(argdata, posix=True)
        lex.whitespace_split = True
        lex.quotes = '"`'
        lex.escapedquotes = ""
        lex.escape = ""
        lex.commenters = ""
        args = list(lex)
        if len(args) not in nlen(expected):
            raise ArgumentBotException("Invalid number of arguments")
        return map(lambda x: do_type(*x), zip(args, expected))

def to_signed(n):
    return c_long(n).value
def to_unsigned(n):
    return c_ulong(n).value

def lower_if_not_none(s):
    return s.lower() if s else None
