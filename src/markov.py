from peewee import fn
from random import random
from re import compile
from .model import db_proxy
from .models import MarkovModel, UserModel
from .util import lower_if_not_none

RE_SPLIT = compile(r"\s+")
RE_REPLACE = compile(r"[?!.,:;]+|[()]")

def split_message(message):
    return RE_SPLIT.split(RE_REPLACE.sub(
        lambda g: " %s " % g.group(0),
        message
    ).strip())

def add_to_dict(d, a, b):
    if a in d:
        if b in d[a]:
            d[a][b] += 1
        else:
            d[a][b] = 1
    else:
        d[a] = {b: 1}

def train_markov(user, data):
    UserModel.get_balance(user)
    delta = {}
    if data == [""]:
        return
    last = None
    for current in data:
        add_to_dict(delta, lower_if_not_none(last), current)
        last = current
    add_to_dict(delta, lower_if_not_none(last), None)
    with db_proxy.atomic():
        for a, d in delta.items():
            for b, w in d.items():
                MarkovModel.add(user, a, b, w)

def generate_text(user, start=None):
    UserModel.get_balance(user)
    if not start:
        last = None
        for x in MarkovModel.select().where(
                MarkovModel.user == user,
                MarkovModel.current == None
            ).order_by(fn.Random()).limit(1):
            last = x.next
        if not last:
            return None
    else:
        last = start
    s = last

    while len(s) < 2000:
        l = MarkovModel.select().where(
            MarkovModel.user == user,
            MarkovModel.current == lower_if_not_none(last)
        )
        total = 0
        for x in l:
            total += x.weight
        last = None
        found = False
        for x in l:
            if random() < x.weight / total:
                last = x.next
                found = True
                break
        if not found:
            for x in l.order_by(fn.Random()).limit(1):
                last = x.next
        if not last:
            break
        
        s += " %s" % last
    
    return s
