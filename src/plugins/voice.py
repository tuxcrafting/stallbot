from os import remove
from random import randint
from subprocess import call
from traceback import print_exc
from ..plugin import command, description, Plugin
from ..util import BotException, UnauthorizedBotException

class VoicePlugin(Plugin):
    name = "Voice Commands"
    sname = "voice"

    def __init__(self, bot):
        super(VoicePlugin, self).__init__(bot)
        self.vclients = {}
        self.vqueue = {}

    @command("vjoin")
    @description("Join a voice channel")
    async def cmd_vjoin(self, msg):
        if msg.server is None:
            await self.bot.send_message(
                msg.channel,
                "Can't connect here!"
            )
            return
        if msg.author.voice.voice_channel is None:
            await self.bot.send_message(
                msg.channel,
                "You aren't connected in any voice channel"
            )
            return
        if msg.server.id in self.vclients:
            await self.bot.send_message(
                msg.channel,
                "I'm already connected to a voice channel in this server!"
            )
            return
        v = await self.bot.join_voice_channel(msg.author.voice.voice_channel)
        self.vclients[msg.server.id] = v
        self.vqueue[msg.server.id] = []
        await self.bot.send_message(msg.channel, "Connected!")

    @command("vleave")
    @description("Leave a voice channel")
    async def cmd_vleave(self, msg):
        if msg.server is None:
            await self.bot.send_message(
                msg.channel,
                "Can't connect here!"
            )
            return
        if msg.server.id not in self.vclients:
            await self.bot.send_message(
                msg.channel,
                "I'm not connected to any voice channel in this server!"
            )
            return
        await self.vclients[msg.server.id].disconnect()
        del self.vclients[msg.server.id]
        del self.vqueue[msg.server.id]
        await self.bot.send_message(msg.channel, "Disconnected!")

    @command("vsay")
    @description("Make me say something... out loud")
    async def cmd_vsay(self, msg, message:str):
        voice = "default"
        if not message:
            raise BotException("I can't say an empty message!")
        if msg.server is None:
            await self.bot.send_message(
                msg.channel,
                "Can't connect here!"
            )
            return
        if msg.server.id not in self.vclients:
            await self.bot.send_message(
                msg.channel,
                "I'm not connected to any voice channel in this server!"
            )
            return
        if self.vqueue[msg.server.id]:
            self.vqueue[msg.server.id].append((voice, message))
        else:
            self.vqueue[msg.server.id].append((voice, message))
            self.__play_espeak(msg)

    def __after(self, msg, f):
        def g(player):
            try:
                if msg.server.id in self.vqueue:
                    self.vqueue[msg.server.id].pop(0)
                remove(player.V_filename)
                if msg.server.id in self.vqueue and self.vqueue[msg.server.id]:
                    self.__play_espeak(msg)
            except:
                print_exc()
        return g
    
    def __play_espeak(self, msg):
        f = "/tmp/espeak%d.wav" % randint(0, 10000)
        voice, m = self.vqueue[msg.server.id][0]
        c = call(["espeak", "-w", f, "-v", voice, m])
        if c:
            self.vqueue[msg.server.id].pop(0)
            if msg.server.id in self.vqueue and self.vqueue[msg.server.id]:
                self.__play_espeak(msg)
        else:
            player = self.vclients[msg.server.id].create_ffmpeg_player(
                f,
                after=self.__after(msg, f)
            )
            player.start()
            player.V_filename = f
