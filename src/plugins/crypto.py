from hashlib import algorithms_available, new
from os import urandom
from ..plugin import command, description, Plugin
from ..util import BotException

class CryptoPlugin(Plugin):
    name = "Cryptography Commands"
    sname = "crypto"

    @command("hash")
    @description(
        f"Hash some data. Supported algorithms:"
        + f"{', '.join(map(lambda x: f'`{x}`', algorithms_available))}"
    )
    async def cmd_hash(
            self,
            msg,
            algorithm:str,
            data:str,
            digestlen:[int]=None
        ):
        if algorithm not in algorithms_available:
            raise BotException(f"`{algorithm}` is not a supported algorithm")
        h = new(algorithm)
        h.update(data.encode("utf8"))
        if algorithm.lower().startswith("shake"):
            try:
                if int(digestlen) > 1000:
                    raise BotException("Too long for discord")
                if int(digestlen) <= 0:
                    raise BotException("The length must be >= 1")
                digest = h.hexdigest(int(digestlen))
            except ValueError:
                raise BotException("Invalid number format")
            except TypeError:
                raise BotException("SHAKE hashes needs a digest length")
        else:
            digest = h.hexdigest()
        await self.bot.send_message(msg.channel, digest)

    @command("random", "urandom")
    @description("Generate some cryptographically secure bytes")
    async def cmd_random(self, msg, length:int):
        try:
            if int(length) > 1000:
                raise BotException("Too long for discord")
            if int(length) <= 0:
                raise BotException("The length must be >= 1")
            data = urandom(int(length))
            txt = "".join(map("%02x".__mod__, data))
            await self.bot.send_message(msg.channel, txt)
        except ValueError:
            raise BotException("Invalid number format")
