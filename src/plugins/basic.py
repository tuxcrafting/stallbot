from datetime import datetime
from discord import Embed
from inspect import signature
from time import time
from ..plugin import command, description, Plugin
from ..util import BotException, UnauthorizedBotException

class BasicPlugin(Plugin):
    name = "Basic Commands"
    sname = "basic"

    @command("help", "commands")
    @description("Help message")
    async def cmd_help(self, msg, pluginOrCommand:[str]=None):
        if not pluginOrCommand:
            embed = Embed(
                title="All Commands - Stallbot 2.0",
                color=msg.author.roles[-1].color
            )
            for plugin in self.bot.plugins.values():
                if "botadmin" in dir(plugin) \
                    and msg.author.id != self.bot.config.owner_id:
                    continue
                embed.add_field(
                    name=f"`{plugin.sname}` - {plugin.name}",
                    value=", ".join(
                        map(
                            lambda x: ", ".join(
                                map(
                                    lambda y: f"`{y}`",
                                    x.triggers
                                )
                            ),
                            plugin.commands.values()
                        )
                    ),
                    inline=True
                )
        elif pluginOrCommand in self.bot.plugins.keys():
            plugin = self.bot.plugins[pluginOrCommand]
            if "botadmin" in dir(plugin) \
                and msg.author.id != self.bot.config.owner_id:
                raise UnauthorizedBotException()
            embed = Embed(
                title=f"{plugin.name} - Stallbot 2.0",
                color=msg.author.roles[-1].color
            )
            for k, command in plugin.commands.items():
                embed.add_field(
                    name=", ".join(map(lambda x: f"`{x}`", command.triggers)),
                    value=command.description,
                    inline=True
                )
        else:
            ok = False
            for plugin in self.bot.plugins.values():
                for k, command in plugin.commands.items():
                    if pluginOrCommand in command.triggers:
                        if "botadmin" in dir(plugin) \
                            and msg.author.id != self.bot.config.owner_id:
                            raise UnauthorizedBotException()
                        embed = Embed(
                            title=f"`{k}` - Stallbot 2.0",
                            description=command.description,
                            color=msg.author.roles[-1].color
                        )
                        embed.add_field(
                            name="Triggers",
                            value=", ".join(
                                map(
                                    lambda x: f"`{x}`",
                                    command.triggers
                                )
                            ),
                            inline=True
                        )
                        usage = []
                        for x in list(
                                signature(command).parameters.keys()
                            )[1:]:
                            t = command.__annotations__[x]
                            if isinstance(t, list):
                                t = f"[{t[0].__name__}]"
                            else:
                                t = t.__name__
                            usage.append(f"{x}:{t}")
                        susage = " ".join(usage)
                        embed.add_field(
                            name="Usage",
                            value=f"`{self.bot.config.prefix}{k} {susage}`",
                            inline=True
                        )
                        ok = True
            if not ok:
                raise BotException(
                    f"Unknown command or plugin `{pluginOrCommand}`"
                )
        await self.bot.send_message(msg.channel, embed=embed)

    @command("ping")
    @description("Pong!")
    async def cmd_ping(self, msg):
        ta = time()
        pmsg = await self.bot.send_message(msg.channel, "Pinging...")
        tb = time()
        delta = (tb - ta) * 1000
        await self.bot.edit_message(pmsg, f"Pong! {delta:.2f} ms")

    @command("say")
    @description("Make me say something")
    async def cmd_say(self, msg, message:str):
        if not message:
            raise BotException("I can't say an empty message!")
        await self.bot.send_message(msg.channel, message)

    @command("uptime")
    @description("Get my uptime")
    async def cmd_uptime(self, msg):
        uptime = datetime.now() - self.bot.start_time
        await self.bot.send_message(msg.channel, f"Uptime: {uptime}")
