__all__ = ["BasicPlugin", "BotAdminPlugin", "CryptoPlugin", "FunPlugin",
           "MathPlugin", "MoneyPlugin", "VoicePlugin"]

from .basic import BasicPlugin
from .botadmin import BotAdminPlugin
from .crypto import CryptoPlugin
from .fun import FunPlugin
from .math import MathPlugin
from .money import MoneyPlugin
from .voice import VoicePlugin
