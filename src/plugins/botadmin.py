from asyncio import sleep
from datetime import datetime, timedelta
from discord import Game
from random import choice
from threading import Thread
from ..models import UserModel, TransactionModel
from ..plugin import command, description, Plugin

class BotAdminPlugin(Plugin):
    name = "Bot Administration Commands"
    sname = "botadmin"

    botadmin = 1

    def __init__(self, bot):
        super(BotAdminPlugin, self).__init__(bot)

    async def on_ready(self):
        self.bot.client.loop.create_task(self.status_thread())
        await self.change_status()

    async def status_thread(self):
        await self.bot.client.wait_until_ready()
        self.next = datetime.utcnow()
        while self.next.minute % 5:
            self.next += timedelta(minutes=1)
        while not self.bot.client.is_closed:
            if datetime.utcnow() >= self.next:
                await self.change_status()
                self.next += timedelta(minutes=10)
            await sleep(10)

    async def change_status(self):
        status = choice(self.statuslines)
        await self.bot.change_presence(Game(name=status))

    @command("exit", "quit")
    @description("Exit the bot")
    async def cmd_exit(self, msg):
        await self.bot.send_message(msg.channel, "Bye!")
        await self.bot.stop()

    @command("nick")
    @description("Change my nickname")
    async def cmd_nick(self, msg, nick:str):
        await self.bot.client.change_nickname(
            msg.server.get_member(self.bot.client.user.id),
            nick
        )
        await self.bot.send_message(msg.channel, "Changed nickname!")

    @command("setbalance", "setbal")
    @description("Set the balance of someone")
    async def cmd_setbalance(self, msg, user:id, balance:int):
        user = await self.bot.get_user_info(user)
        TransactionModel.create(
            user.id,
            balance - UserModel.get_balance(user.id)
        )
        UserModel.set_balance(user.id, balance)
        await self.bot.send_message(
            msg.channel,
            f"{user}'s balance is now S₡{balance}!"
        )

    @command("flush")
    @description("Flush the pending payouts")
    async def cmd_flush(self, msg):
        self.bot.plugins["money"].flush_pending()
        await self.bot.send_message(msg.channel, "Flushed pending payouts!")

    @command("status")
    @description("Change the current status")
    async def cmd_status(self, msg):
        await self.change_status()
        await self.bot.send_message(msg.channel, "Changed current status!")

    @command("rep")
    @description("Repeat something (made to fuck with DesuBot)")
    async def cmd_rep(self, msg, number:int, say:str):
        for i in range(number):
            await self.bot.send_message(msg.channel, say)
            sleep(0.5)
