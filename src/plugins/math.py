from os import remove
from random import randint
from sympy import preview
from ..bot import BotException
from ..plugin import command, description, Plugin

class MathPlugin(Plugin):
    name = "Math Commands"
    sname = "math"

    @command("latex")
    @description("Render a LaTeX expression")
    async def cmd_latex(self, msg, code:str):
        try:
            filename = f"/tmp/latex{randint(0, 1000)}.png"
            preview(
                r"\fontsize{18}{20}\selectfont$$%s$$" % code,
                viewer="file",
                filename=filename,
                euler=False
            )
            await self.bot.send_file(
                msg.channel,
                filename,
                filename="latex.png"
            )
            remove(filename)
        except RuntimeError:
            raise BotException("LaTeX exited abnormally")
