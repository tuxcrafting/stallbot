from asyncio import sleep
from datetime import datetime, timedelta
from discord import Embed
from ..model import db_proxy
from ..models import UserModel, TransactionModel
from ..plugin import command, description, Plugin
from ..util import BotException

class MoneyPlugin(Plugin):
    REWARD_MESSAGE = 20

    name = "StallCoin Commands"
    sname = "money"

    def __init__(self, bot):
        super(MoneyPlugin, self).__init__(bot)
        self.pending = {}

    async def on_ready(self):
        self.bot.client.loop.create_task(self.payout_thread())

    async def payout_thread(self):
        await self.bot.client.wait_until_ready()
        self.next = datetime.utcnow()
        while self.next.minute:
            self.next += timedelta(minutes=1)
        while not self.bot.client.is_closed:
            if datetime.utcnow() >= self.next:
                self.flush_pending()
                self.next += timedelta(hours=1)
            await sleep(10)

    def flush_pending(self):
        with db_proxy.atomic():
            for k, v in self.pending.items():
                UserModel.set_balance(
                    k,
                    UserModel.get_balance(k) + v
                )
                TransactionModel.create(k, v)
        self.pending.clear()

    @command("balance", "bal")
    @description("Get your current balance")
    async def cmd_balance(self, msg, user:[id]=None):
        if user:
            user = await self.bot.get_user_info(user)
        else:
            user = msg.author
        embed = Embed(
            title=f"{user} - StallBank",
            thumbnail=msg.author.avatar,
            color=msg.author.roles[-1].color
        )
        embed.add_field(
            name="Balance",
            value=f"S₡{UserModel.get_balance(user.id)}"
        )
        await self.bot.send_message(msg.channel, embed=embed)

    @command("send", "transfer")
    @description("Send some S₡ to someone")
    async def cmd_send(self, msg, destination:id, amount:int):
        if amount < 1:
            raise BotException("You can't send less than S₡1!")
        elif destination == msg.author.id:
            raise BotException("You can't send money to yourself...")
        elif UserModel.get_balance(msg.author.id) < amount:
            raise BotException("You don't have enough money")
        destination = await self.bot.get_user_info(destination)
        UserModel.set_balance(
            msg.author.id,
            UserModel.get_balance(msg.author.id) - amount
        )
        UserModel.set_balance(
            destination.id,
            UserModel.get_balance(destination.id) + amount
        )
        TransactionModel.send(msg.author.id, destination.id, amount)
        await self.bot.send_message(
            msg.channel,
            f"Sent S₡{amount} to {destination}"
        )
