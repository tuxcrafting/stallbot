from random import choice
from ..markov import generate_text
from ..plugin import command, description, Plugin

class FunPlugin(Plugin):
    name = "Fun (maybe) commands"
    sname = "fun"

    @command("interject", "linux")
    @description("*I'd just like to interject for a moment...*")
    async def cmd_interject(self, msg):
        with open("copypastas/interjection.txt") as f:
            await self.bot.send_message(msg.channel, f.read())

    @command("navyseals")
    @description("When you graduate top of your class")
    async def cmd_navyseals(self, msg):
        with open("copypastas/navyseals.txt") as f:
            await self.bot.send_message(msg.channel, f.read())

    @command("rickmorty", "wubbalubbadubdub")
    @description("When someone has less than 400 IQ points")
    async def cmd_rickmorty(self, msg):
        with open("copypastas/rickandmorty.txt") as f:
            await self.bot.send_message(msg.channel, f.read())

    @command("despacito")
    @description("This is so sad Stallbot play Despacito")
    async def cmd_rickmorty(self, msg):
        with open("copypastas/despacito.txt") as f:
            await self.bot.send_message(msg.channel, f.read())

    @command("diamonds")
    @description("Aha - Take On Me but improved")
    async def cmd_rickmorty(self, msg):
        with open("copypastas/minediamonds.txt") as f:
            await self.bot.send_message(msg.channel, f.read())

    answers_8ball = [
        "It is decidedly so.",
        "Without a doubt.",
        "Yes, definitely.",
        "You may rely on it.",
        "As I see it, yes.",
        "Outlook good.",
        "Yes.",
        "Signs point to yes.",
        "Reply hazy, try again.",
        "Ask again later.",
        "Better not tell you now.",
        "Cannot predict now.",
        "Concentrate and ask again.",
        "Don't count on it.",
        "My reply is no.",
        "My sources say no.",
        "Outlook not so good.",
        "Very doubtful."
    ]

    @command("8ball")
    @description("Ask the 8-ball")
    async def cmd_8ball(self, msg, question:str):
        await self.bot.send_message(msg.channel, choice(self.answers_8ball))

    @command("jesus")
    @description("Show an image of jesus")
    async def cmd_jesus(self, msg):
        await self.bot.send_message(msg.channel, "https://tuxcrafting.cf/data/IGGHBJQJVH.jpg")

    @command("markov")
    @description("Tries to simulate you based on your past messages using a probabilistic model")
    async def cmd_markov(self, msg, user:[id]=None, start:[str]=None):
        if not user:
            user = int(msg.author.id)
        try:
            s = generate_text(user, start)
        except:
            await self.bot.send_message(msg.channel, "Could not generate text")
        if s:
            await self.bot.send_message(msg.channel, s)
        else:
            await self.bot.send_message(msg.channel, "Could not generate text")
