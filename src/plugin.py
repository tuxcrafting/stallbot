from collections import OrderedDict

def command(*triggers):
    def d(f):
        f.triggers = triggers
        return f
    return d

def description(desc):
    def d(f):
        f.description = desc
        return f
    return d

class Plugin:
    def __init__(self, bot):
        self.bot = bot
        self.commands = {}
        for x in filter(lambda x: x.startswith("cmd_"), dir(self)):
            self.commands[x[4:]] = getattr(self, x)

    def name(self):
        return ""

    async def on_ready(self):
        pass
