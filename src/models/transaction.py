from os import urandom
from peewee import BigIntegerField, ForeignKeyField, TimestampField
from struct import unpack
from .user import UserModel
from ..model import BaseModel
from ..util import to_signed

class TransactionModel(BaseModel):
    id = BigIntegerField(primary_key=True)
    timestamp = TimestampField(resolution=1000, utc=True)
    source = ForeignKeyField(UserModel, related_name="txsource", null=True)
    destination = ForeignKeyField(UserModel, related_name="txdestination")
    amount = BigIntegerField()

    @staticmethod
    def send(source, destination, amount):
        source = to_signed(int(source))
        destination = to_signed(int(destination))
        tx = TransactionModel(
            id=unpack("<q", urandom(8))[0],
            source=UserModel.get(id=source),
            destination=UserModel.get(id=destination),
            amount=amount
        )
        tx.save(force_insert=True)

    @staticmethod
    def create(destination, amount):
        destination = to_signed(int(destination))
        tx = TransactionModel(
            id=unpack("<q", urandom(8))[0],
            source=None,
            destination=UserModel.get(id=destination),
            amount=amount
        )
        tx.save(force_insert=True)
