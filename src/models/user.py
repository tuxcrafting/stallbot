from peewee import BigIntegerField
from ..model import BaseModel
from ..util import to_signed

class UserModel(BaseModel):
    id = BigIntegerField(primary_key=True)
    balance = BigIntegerField()

    @staticmethod
    def get_balance(id):
        id = to_signed(int(id))
        user = UserModel.get_or_create(
            id=id,
            defaults={
                "id": id,
                "balance": 0
            }
        )
        if user[1]:
            user[0].save()
        return user[0].balance

    @staticmethod
    def set_balance(id, balance):
        id = to_signed(int(id))
        user = UserModel.get_or_create(
            id=id,
            defaults={
                "id": id,
                "balance": balance
            }
        )
        if not user[1]:
            user[0].balance = balance
        user[0].save()
