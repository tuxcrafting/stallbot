from os import urandom
from peewee import BigIntegerField, CharField, ForeignKeyField
from struct import unpack
from .user import UserModel
from ..model import BaseModel
from ..util import to_signed

class MarkovModel(BaseModel):
    id = BigIntegerField(primary_key=True)
    user = ForeignKeyField(UserModel, related_name="markov")
    current = CharField(2000, null=True)
    next = CharField(2000, null=True)
    weight = BigIntegerField()

    @staticmethod
    def add(user, current, next, weight=1):
        user = to_signed(user)
        m = MarkovModel.get_or_create(
            user=user,
            current=current,
            next=next,
            defaults={
                "id": unpack("<q", urandom(8))[0],
                "user": UserModel.get(id=user),
                "current": current,
                "next": next,
                "weight": 0,
            }
        )
        m[0].weight += weight
        m[0].save()
