from collections import OrderedDict
from datetime import datetime
from discord import Client, NotFound
from inspect import signature
from logging import info
from traceback import format_exc
from .markov import split_message, train_markov
from .util import BotException, CmdNotFoundException, parse_args, \
        UnauthorizedBotException

class Bot:
    def __init__(self, config, database):
        self.config = config
        self.database = database
        self.client = Client()
        self.client.event(self.on_message)
        self.client.event(self.on_ready)
        self.plugins = OrderedDict()
        self.buffer = False

    def run(self):
        self.start_time = datetime.now()
        self.client.run(self.config.token)

    async def stop(self):
        await self.client.logout()

    def add_plugin(self, plugin):
        self.plugins[plugin.sname] = plugin(self)

    async def send_message(self, channel, content=None, embed=None):
        if self.buffer:
            self.buffer_data += content
        else:
            if content:
                while len(content) > 2000:
                    await self.client.send_message(
                        channel,
                        content=content[:2000]
                    )
                    content = content[2000:]
                if not content:
                    content = None
            return await self.client.send_message(
                channel,
                content=content,
                embed=embed
            )
    def send_file(self, channel, data, filename=None):
        return self.client.send_file(channel, data, filename=filename)
    def edit_message(self, msg, content=None, embed=None):
        return self.client.edit_message(msg, new_content=content, embed=embed)
    def get_user_info(self, user_id):
        return self.client.get_user_info(user_id)
    def change_presence(self, game=None):
        return self.client.change_presence(game=game)
    def join_voice_channel(self, channel):
        return self.client.join_voice_channel(channel)

    async def on_ready(self):
        for plugin in self.plugins.values():
            await plugin.on_ready()

    async def on_message(self, msg):
        pending = self.plugins["money"].pending
        pending[msg.author.id] = pending.get(msg.author.id, 0) \
                               + self.plugins["money"].REWARD_MESSAGE

        if not msg.content.startswith(self.config.prefix) or msg.author.bot:
            train_markov(int(msg.author.id), split_message(msg.content))
            return

        if msg.server:
            info(
                f"{msg.server.id} #{msg.channel.id} "
                + f"<{msg.author.id}> {msg.content}"
            )
        else:
            info(f"#{msg.channel.id} <{msg.author.id}> {msg.content}")

        try:
            await self.exec_str(msg, msg.content[len(self.config.prefix):])
        except BotException as ex:
            await self.send_message(msg.channel, str(ex))
        except NotFound as ex:
            await self.send_message(msg.channel, "Can't find specified user!")
        except Exception as ex:
            await self.send_message(
                msg.channel,
                f"Something went wrong!\n```\n{format_exc()}\n```"
            )

    async def exec_str(self, msg, cmdline, buffer=False):
        cmd = ""
        argdata = ""
        pushingarg = False
        for c in cmdline:
            if c == " ":
                pushingarg = True
            if pushingarg:
                argdata += c
            else:
                cmd += c
        argdata = argdata.strip()

        for plugin in self.plugins.values():
            for f in plugin.commands.values():
                if cmd in f.triggers:
                    if "botadmin" in dir(plugin) \
                        and msg.author.id != self.config.owner_id:
                        raise UnauthorizedBotException()
                    e = []
                    for x in list(signature(f).parameters.keys())[1:]:
                        e.append(f.__annotations__[x])
                    args = await parse_args(self, msg, argdata, e)
                    self.buffer = buffer
                    self.buffer_data = ""
                    await f(msg, *args)
                    if buffer:
                        return self.buffer_data
                    return
        raise CmdNotFoundException(cmd)
